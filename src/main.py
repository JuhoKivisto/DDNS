from ip_request import IPRequest
import time

SLEEP_TIME = 60

def main():

    while True:
        ipreq = IPRequest()

        # print("Has changed: ", ipreq.has_changed())
        # print("Has expired: ", ipreq.has_expired())

        if ipreq.has_changed() or ipreq.has_expired():
            ipreq.publish_ip()

        # print(f"Wait for {SLEEP_TIME} seconds")
        time.sleep(SLEEP_TIME)


if __name__ == "__main__":
    main()


